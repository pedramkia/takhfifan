window.Vue = require('../../../node_modules/vue/dist/vue');
import Vuex from 'vuex'
import Toasted from 'vue-toasted';

Vue.use(Vuex)
Vue.use(Toasted)

const store = new Vuex.Store({
    state:{
        cart:[],
        count:0,
        sort:'createdAt'
    },
    mutations:{
        addToCart(state, product) {
            Vue.toasted.show("" +
                product.title + " به سبد خرید شما اضفه شد!", {
                theme: "toasted-primary",
                position: "top-right",
                duration : 5000
            });
            this.state.count++
            let index = this.state.cart.indexOf(product)
            if(index ===-1){
                product.count = 1
                this.state.cart.push(product)
            }else {
                this.state.cart[index].count++
            }
        },
        removeFromCart(state, product) {
            this.state.count--
            let index = this.state.cart.indexOf(product)
            if(this.state.cart[index].count>1){
                this.state.cart[index].count--
            }else{
                this.state.cart.splice(index,1)
            }
        },
        sort(state, by) {
            this.state.sort = by
        },
    }
})
export default store
