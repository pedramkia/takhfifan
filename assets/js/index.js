import '../sass/app.scss';


window.Vue = require('../../node_modules/vue/dist/vue');
import BootstrapVue from 'bootstrap-vue'
import store from './store/index'

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


Vue.component('products', require('./components/products.vue').default);
Vue.component('site-header', require('./components/master/siteHeader.vue').default);


Vue.use(BootstrapVue)
const app = new Vue({
    el: '#app',
    store
});

Vue.filter('characterLimit',(content , count)=>{
    if(content){
        content = content.toString()
        if(content.length > count)
            return content.substr(0,count)+'...'
        else
            return content
    }
})
